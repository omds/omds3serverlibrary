package at.vvo.omds;

import org.apache.cxf.message.Message;
import org.apache.cxf.security.SecurityContext;

import java.util.logging.Logger;

public class EndpointUtils {

    private static final Logger LOGGER = Logger.getLogger(EndpointUtils.class.getName());

    public static String extractUsername(Message message) {
        String username = (String) message.get("Username");
        LOGGER.info("Entered username: " + username);
        SecurityContext sc = message.get(SecurityContext.class);
        if (sc != null) {
            username = sc.getUserPrincipal().getName();
        }
        return username;
    }

}
