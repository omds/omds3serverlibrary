package at.vvo.omds.r1_11_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.*;

public interface UnfallDataFetcher {

    CreateApplicationUnfallResponseType createApplicationUnfall(CreateApplicationUnfallRequestType parameters);

    CreateOfferUnfallResponseType createOfferUnfall(CreateOfferUnfallRequestType parameters);

    CalculateUnfallResponseType calculateUnfall(CalculateUnfallRequestType parameters);

    SubmitApplicationUnfallResponseType submitApplicationUnfall(SubmitApplicationUnfallResponseType parameters);



}
