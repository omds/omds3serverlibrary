package at.vvo.omds.r1_11_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.rs.*;

public interface RechtsSchutzDataFetcher {

    CalculateRechtsschutzResponse calculateRechtsschutz(CalculateRechtsschutzRequest parameters);

    CreateApplicationRechtsschutzResponse createApplicationRechtsschutz(CreateApplicationRechtsschutzRequest parameters);

    CreateOfferRechtsschutzResponse createOfferRechtsschutz(CreateOfferRechtsschutzRequest parameters);

    SubmitApplicationRechtsschutzResponse submitApplicationRechtsschutz(SubmitApplicationRechtsschutzResponse parameters);

}
