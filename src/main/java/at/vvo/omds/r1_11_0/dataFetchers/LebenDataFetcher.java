package at.vvo.omds.r1_11_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.*;

public interface LebenDataFetcher {

    CalculateLebenResponseType calculateLeben(CalculateLebenRequestType parameters);

    SubmitApplicationLebenResponseType submitApplicationLeben(SubmitApplicationLebenResponseType parameters);

    CreateApplicationLebenResponseType createApplicationLeben(CreateApplicationLebenRequestType parameters);

    CreateOfferLebenResponseType createOfferLeben(CreateOfferLebenRequestType parameters);

}
