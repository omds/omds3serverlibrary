package at.vvo.omds.r1_11_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.*;

public interface SchadenDataFetcher {

    SearchClaimResponseType searchClaim(SearchClaimRequestType parameters);

    //InitiateClaimResponseType initiateClaim(InitiateClaimRequestType parameters);

    SubmitClaimResponseType submitClaim(SubmitClaimRequestType parameters);

    //GetClaimResponseLightType getClaimLight(SpezifikationSchadenType parameters);

    //ChangedClaimsListResponseType getChangedClaimsList(ChangedClaimsListRequestType parameters);

    GetClaimResponseType getClaim(GetClaimRequestType parameters);

    //LossEventListResponseType getLossEventList(LossEventListRequestType parameters);

    CheckClaimResponseType checkClaim(CheckClaimRequestType parameters);

}
