package at.vvo.omds.r1_11_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.*;

public interface ServiceTypesDataFetcher {

    OMDSPackageListResponse getOMDSPackageList(OMDSPackageListRequest parameters);

    ArcImageResponse getArcImage(ArcImageRequest parameters);

    DeepLinkBusinessObjectResponse deepLinkClaim(DeepLinkClaimRequest parameters);

    OMDSPackageResponse getOMDSPackage(OMDSPackageRequest parameters);

    DeepLinkBusinessObjectResponse login(LoginRequestType parameters);

    DeepLinkBusinessObjectResponse deepLinkOffer(DeepLinkOfferRequest parameters);

    DeepLinkBusinessObjectResponse deepLinkPartner(DeepLinkPartnerRequest parameters);

    DeepLinkBusinessObjectResponse deepLinkPolicy(DeepLinkPolicyRequest parameters);

    ArcImageInfosResponse getArcImageInfos(ArcImageInfosRequest parameters);

    UserDataResponse getUserData(UserDataRequest parameters);

}
