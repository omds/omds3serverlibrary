package at.vvo.omds.r1_11_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.*;

public interface SachPrivatDataFetcher {

    CalculateSachPrivatResponseType calculateSachPrivat(CalculateSachPrivatRequestType parameters);

    CreateOfferSachPrivatResponseType createOfferSachPrivat(CreateOfferSachPrivatRequestType parameters);

    CreateApplicationSachPrivatResponseType createApplicationSachPrivat(CreateApplicationSachPrivatRequestType parameters);

    SubmitApplicationSachPrivatResponseType submitApplicationSachPrivat(SubmitApplicationSachPrivatRequestType parameters);

}
