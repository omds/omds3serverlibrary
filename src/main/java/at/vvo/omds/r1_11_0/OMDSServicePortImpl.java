package at.vvo.omds.r1_11_0;

import at.vvo.omds.r1_11_0.dataFetchers.*;
import at.vvo.omds.types.omds3Types.r1_11_0.on1basis.*;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.ConversionProposalRequest;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.ConversionProposalResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.ConversionScopeRequest;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.common.ConversionScopeResponse;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kfz.*;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.kranken.*;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.rs.*;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.sachPrivat.*;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.unfall.*;
import at.vvo.omds.types.omds3Types.r1_11_0.on2antrag.leben.*;
//import at.vvo.omds.types.omds3Types.r1_11_0.on3vertrag.*;
//import at.vvo.omds.types.omds3Types.r1_11_0.on4partner.*;
import at.vvo.omds.types.omds3Types.r1_11_0.on7schaden.*;
import at.vvo.omds.types.omds3Types.r1_11_0.service.OmdsServicePortType;
import at.vvo.omds.types.omds3Types.r1_11_0.service.ServiceFaultMsg;
import at.vvo.omds.types.omds3Types.r1_11_0.servicetypes.*;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;
import java.util.logging.Logger;

public class OMDSServicePortImpl implements OmdsServicePortType {

    @Resource
    protected WebServiceContext wsContext;

    private static final Logger LOGGER = Logger.getLogger(OMDSServicePortImpl.class.getName());

    private ServiceTypesDataFetcher serviceTypesDataFetcher;
    private BasisDataFetcher basisDataFetcher;
    private PartnerDataFetcher partnerDataFetcher;
    private SachPrivatDataFetcher sachPrivatDataFetcher;
    private RechtsSchutzDataFetcher rechtsSchutzDataFetcher;
    private KfzDataFetcher kfzDataFetcher;
    private SchadenDataFetcher schadenDataFetcher;
//    private VertragDataFetcher vertragDataFetcher;
    private LebenDataFetcher lebenDataFetcher;
    private UnfallDataFetcher unfallDataFetcher;

    public OMDSServicePortImpl() {
    }

    protected void init(DataFetcherFactory dataFetcherFactory) {
        serviceTypesDataFetcher = dataFetcherFactory.generateServiceTypesDataFetcher();
        basisDataFetcher = dataFetcherFactory.generateBasisDataFetcher();
        partnerDataFetcher = dataFetcherFactory.generatePartnerDataFetcher();
        sachPrivatDataFetcher = dataFetcherFactory.generateSachPrivatDataFetcher();
        rechtsSchutzDataFetcher = dataFetcherFactory.generateRechtsSchutzDataFetcher();
        kfzDataFetcher = dataFetcherFactory.generateKfzDataFetcher();
        schadenDataFetcher = dataFetcherFactory.generateSchadenDataFetcher();
//        vertragDataFetcher = dataFetcherFactory.generateVertragDataFetcher();
        lebenDataFetcher = dataFetcherFactory.generateLebenDataFetcher();
        unfallDataFetcher = dataFetcherFactory.generateUnfallDataFetcher();
    }

    /************************************************ SERVICE TYPES ************************************************/

    public OMDSPackageListResponse getOMDSPackageList(OMDSPackageListRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getOMDSPackageList");

        try {
            return serviceTypesDataFetcher.getOMDSPackageList(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public CreateApplicationKrankenResponse createApplicationKranken(CreateApplicationKrankenRequest param) throws ServiceFaultMsg {
        return null;
    }

    public ArcImageResponse getArcImage(ArcImageRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getArcImage");

        try {
            return serviceTypesDataFetcher.getArcImage(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public AddInformationToClaimResponse addInformationToClaim(AddInformationToClaimRequest param) throws ServiceFaultMsg {
        return null;
    }

    @Override
    public SubmitApplicationRechtsschutzResponse submitApplicationRechtsschutz(SubmitApplicationRechtsschutzRequest param) throws ServiceFaultMsg {
        return null;
    }

    @Override
    public AcknowledgeDocumentsResponse acknowledgeDocuments(AcknowledgeDocumentsRequest param) throws ServiceFaultMsg {
        return null;
    }

    public DeepLinkBusinessObjectResponse deepLinkClaim(DeepLinkClaimRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkClaim");

        try {
            return serviceTypesDataFetcher.deepLinkClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public CheckCoverageResponse checkCoverage(CheckCoverageRequest param) throws ServiceFaultMsg {
        return null;
    }

    @Override
    public SubmitApplicationKrankenResponse submitApplicationKranken(SubmitApplicationKrankenRequest param) throws ServiceFaultMsg {
        return null;
    }

    public OMDSPackageResponse getOMDSPackage(OMDSPackageRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getOMDSPackage");

        try {
            return serviceTypesDataFetcher.getOMDSPackage(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse login(LoginRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation login");

        try {
            return serviceTypesDataFetcher.login(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkOffer(DeepLinkOfferRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkOffer");

        try {
            return serviceTypesDataFetcher.deepLinkOffer(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public CreateVBResponse createVB(CreateVBRequest param) throws ServiceFaultMsg {
        return null;
    }

    public DeepLinkBusinessObjectResponse deepLinkPartner(DeepLinkPartnerRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkPartner");

        try {
            return serviceTypesDataFetcher.deepLinkPartner(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkPolicy(DeepLinkPolicyRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkPolicy");

        try {
            return serviceTypesDataFetcher.deepLinkPolicy(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public ArcImageInfosResponse getArcImageInfos(ArcImageInfosRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getArcImageInfos");

        try {
            return serviceTypesDataFetcher.getArcImageInfos(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public UserDataResponse getUserData(UserDataRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getUserData");

        try {
            return serviceTypesDataFetcher.getUserData(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }


    /************************************************ BASIS ************************************************/

    public AddDocToBusinessCaseResponseType addDocToBusinessCase(AddDocToBusinessCaseRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation addDocToBusinessCase");

        try {
            return basisDataFetcher.addDocToBusinessCase(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public CalculateRechtsschutzResponse calculateRechtsschutz(CalculateRechtsschutzRequest param) throws ServiceFaultMsg {
        return null;
    }

    public GetStateChangesResponseType getStateChanges(GetStateChangesRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getStateChanges");

        try {
            return basisDataFetcher.getStateChanges(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetNumberOfDocumentsResponseType getNumberOfDocuments(GetNumberOfDocumentsRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getNumberOfDocuments");

        try {
            return basisDataFetcher.getNumberOfDocuments(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetDocumentsOfPeriodResponseType getDocumentsOfPeriod(GetDocumentsOfPeriodRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getDocumentsOfPeriod");

        try {
            return basisDataFetcher.getDocumentsOfPeriod(parameters);
        } catch (Exception ex) {

            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetDocumentsOfObjectResponseType getDocumentsOfObject(GetDocumentsOfObjectRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getDocumentsOfObject");

        try {
            return basisDataFetcher.getDocumentsOfObject(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

//    public DeclareEndpointResponseType declareEndpoint(DeclareEndpointRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation declareEndpoint");
//
//        try {
//            return basisDataFetcher.declareEndpoint(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }


    /************************************************ PARTNER ************************************************/

//    public CheckAddressResponseType checkAddress(CheckAddressRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation checkAddress");
//
//        try {
//            return partnerDataFetcher.checkAddress(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public AddCommunicationObjectResponseType addCommunicationObject(AddCommunicationObjectRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation addCommunicationObject");
//
//        try {
//            return partnerDataFetcher.addCommunicationObject(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public ChangePersonDataResponseType changePersonData(ChangePersonDataRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation changePersonData");
//
//        try {
//            return partnerDataFetcher.changePersonData(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public ChangeCommunicationObjectResponse changeCommunicationObject(ChangeCommunicationObjectRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation changeCommunicationObject");
//
//        try {
//            return partnerDataFetcher.changeCommunicationObject(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public DeleteCommunicationObjectResponseType deleteCommunicationObject(DeleteCommunicationObjectRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation deleteCommunicationObject");
//
//        try {
//            return partnerDataFetcher.deleteCommunicationObject(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public GetPartnerResponseType getPartner(GetPartnerRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation getPartner");
//
//        try {
//            return partnerDataFetcher.getPartner(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public ChangePartnerMainAddressResponseType changePartnerMainAddress(ChangePartnerMainAddressRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation changePartnerMainAddress");
//
//        try {
//            return partnerDataFetcher.changePartnerMainAddress(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }


    /************************************************ SACH PRIVAT ************************************************/

    public CalculateSachPrivatResponseType calculateSachPrivat(CalculateSachPrivatRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation calculateSachPrivat");

        try {
            return sachPrivatDataFetcher.calculateSachPrivat(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public CreateApplicationRechtsschutzResponse createApplicationRechtsschutz(CreateApplicationRechtsschutzRequest param) throws ServiceFaultMsg {
        return null;
    }

    public CreateOfferSachPrivatResponseType createOfferSachPrivat(CreateOfferSachPrivatRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createOfferSachPrivat");

        try {
            return sachPrivatDataFetcher.createOfferSachPrivat(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public CreateApplicationSachPrivatResponseType createApplicationSachPrivat(CreateApplicationSachPrivatRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createApplicationSachPrivat");

        try {
            return sachPrivatDataFetcher.createApplicationSachPrivat(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public ConversionScopeResponse conversionScope(ConversionScopeRequest param) throws ServiceFaultMsg {
        return null;
    }

    @Override
    public GetClaimResponseType getClaim(GetClaimRequestType param) throws ServiceFaultMsg {
        return null;
    }

    public SubmitApplicationSachPrivatResponseType submitApplicationSachPrivat(SubmitApplicationSachPrivatRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation submitApplicationSachPrivat");

        try {
            return sachPrivatDataFetcher.submitApplicationSachPrivat(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }


    /************************************************ RECHTSSCHUTZ ************************************************/
//
//    public CalculateRechtsschutzResponseType calculateRechtsschutz(CalculateRechtsschutzRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation calculateRechtsschutz");
//
//        try {
//            return rechtsSchutzDataFetcher.calculateRechtsschutz(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public CreateApplicationRechtsschutzResponseType createApplicationRechtsschutz(CreateApplicationRechtsschutzRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation createApplicationRechtsschutz");
//
//        try {
//            return rechtsSchutzDataFetcher.createApplicationRechtsschutz(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public CreateOfferRechtsschutzResponseType createOfferRechtsschutz(CreateOfferRechtsschutzRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation createOfferRechtsschutz");
//
//        try {
//            return rechtsSchutzDataFetcher.createOfferRechtsschutz(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public SubmitApplicationRechtsschutzResponseType submitApplicationRechtsschutz(SubmitApplicationRechtsschutzResponseType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation submitApplicationRechtsschutz");
//
//        try {
//            return rechtsSchutzDataFetcher.submitApplicationRechtsschutz(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//

    /************************************************ KFZ ************************************************/

    public CreateApplicationKfzResponseType createApplicationKfz(CreateApplicationKfzRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createApplicationKfz");

        try {
            return kfzDataFetcher.createApplicationKfz(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public SubmitReceiptResponse submitReceipt(SubmitReceiptRequest param) throws ServiceFaultMsg {
        return null;
    }

    public CreateOfferKfzResponseType createOfferKfz(CreateOfferKfzRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createOfferKfz");

        try {
            return kfzDataFetcher.createOfferKfz(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public SubmitApplicationLebenResponseType submitApplicationLeben(SubmitApplicationLebenRequestType param) throws ServiceFaultMsg {
        return null;
    }

    @Override
    public CreateOfferRechtsschutzResponse createOfferRechtsschutz(CreateOfferRechtsschutzRequest param) throws ServiceFaultMsg {
        return null;
    }

    public CalculateKfzResponseType calculateKfz(CalculateKfzRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation calculateKfz");

        try {
            return kfzDataFetcher.calculateKfz(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public CreateOfferKrankenResponse createOfferKranken(CreateOfferKrankenRequest param) throws ServiceFaultMsg {
        return null;
    }

    public SubmitApplicationKfzResponseType submitApplicationKfz(SubmitApplicationKfzRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation submitApplicationKfz");

        try {
            return kfzDataFetcher.submitApplicationKfz(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }


    /************************************************ SCHADEN ************************************************/

    public SearchClaimResponseType searchClaim(SearchClaimRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation searchClaim");

        try {
            return schadenDataFetcher.searchClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

//    public InitiateClaimResponseType initiateClaim(InitiateClaimRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation initiateClaim");
//
//        try {
//            return schadenDataFetcher.initiateClaim(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }

    public SubmitClaimResponseType submitClaim(SubmitClaimRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation submitClaim");

        try {
            return schadenDataFetcher.submitClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

//    public GetClaimResponseLightType getClaimLight(SpezifikationSchadenType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation getClaimLight");
//
//        try {
//            return schadenDataFetcher.getClaimLight(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public ChangedClaimsListResponseType getChangedClaimsList(ChangedClaimsListRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation getChangedClaimsList");
//
//        try {
//            return schadenDataFetcher.getChangedClaimsList(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }

//    public GetClaimResponseType getClaim(SpezifikationSchadenType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation getClaim");
//
//        try {
//            return schadenDataFetcher.getClaim(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public LossEventListResponseType getLossEventList(LossEventListRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation getLossEventList");
//
//        try {
//            return schadenDataFetcher.getLossEventList(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }

    public CheckClaimResponseType checkClaim(CheckClaimRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation checkClaim");

        try {
            return schadenDataFetcher.checkClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }


    /************************************************ VERTRAG ************************************************/

//    public CollectionChangeResponseType collectionChange(CollectionChangeRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation collectionChange");
//
//        try {
//            return vertragDataFetcher.collectionChange(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public GetPoliciesOfPartnerResponseType getPoliciesOfPartner(GetPoliciesOfPartnerRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation getPoliciesOfPartner");
//
//        try {
//            return vertragDataFetcher.getPoliciesOfPartner(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }
//
//    public SetMailingAddressResponseType setMailingAddress(SetMailingAddressRequestType parameters) throws ServiceFaultMsg {
//        LOGGER.info("Executing operation setMailingAddress");
//
//        try {
//            return vertragDataFetcher.setMailingAddress(parameters);
//        } catch (Exception ex) {
//            throw new ServiceFaultMsg(ex.getMessage(), ex);
//        }
//    }


    /************************************************ LEBEN ************************************************/

    public CalculateLebenResponseType calculateLeben(CalculateLebenRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation calculateLeben");

        try {
            return lebenDataFetcher.calculateLeben(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public SubmitApplicationLebenResponseType submitApplicationLeben(SubmitApplicationLebenResponseType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation submitApplicationLeben");

        try {
            return lebenDataFetcher.submitApplicationLeben(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public CreateApplicationLebenResponseType createApplicationLeben(CreateApplicationLebenRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createApplicationLeben");

        try {
            return lebenDataFetcher.createApplicationLeben(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public CreateOfferLebenResponseType createOfferLeben(CreateOfferLebenRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createOfferLeben");

        try {
            return lebenDataFetcher.createOfferLeben(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }


    /************************************************ UNFALL ************************************************/

    public CreateApplicationUnfallResponseType createApplicationUnfall(CreateApplicationUnfallRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createApplicationUnfall");

        try {
            return unfallDataFetcher.createApplicationUnfall(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public CalculateKrankenResponse calculateKranken(CalculateKrankenRequest param) throws ServiceFaultMsg {
        return null;
    }

    public CreateOfferUnfallResponseType createOfferUnfall(CreateOfferUnfallRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createOfferUnfall");

        try {
            return unfallDataFetcher.createOfferUnfall(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public CalculateUnfallResponseType calculateUnfall(CalculateUnfallRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation calculateUnfall");

        try {
            return unfallDataFetcher.calculateUnfall(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    @Override
    public SubmitApplicationUnfallResponseType submitApplicationUnfall(SubmitApplicationUnfallRequestType param) throws ServiceFaultMsg {
        return null;
    }

    @Override
    public ConversionProposalResponse conversionProposal(ConversionProposalRequest param) throws ServiceFaultMsg {
        return null;
    }

    public SubmitApplicationUnfallResponseType submitApplicationUnfall(SubmitApplicationUnfallResponseType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation submitApplicationUnfall");

        try {
            return unfallDataFetcher.submitApplicationUnfall(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }


    public abstract class DataFetcherFactory {

        public abstract ServiceTypesDataFetcher generateServiceTypesDataFetcher();

        public abstract BasisDataFetcher generateBasisDataFetcher();

        public abstract PartnerDataFetcher generatePartnerDataFetcher();

        public abstract SachPrivatDataFetcher generateSachPrivatDataFetcher();

        public abstract RechtsSchutzDataFetcher generateRechtsSchutzDataFetcher();

        public abstract KfzDataFetcher generateKfzDataFetcher();

        public abstract SchadenDataFetcher generateSchadenDataFetcher();

        public abstract VertragDataFetcher generateVertragDataFetcher();

        public abstract LebenDataFetcher generateLebenDataFetcher();

        public abstract UnfallDataFetcher generateUnfallDataFetcher();

    }

}
