package at.vvo.omds.r1_3_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_3_0.servicetypes.*;

public interface ServiceTypesDataFetcher {

    OMDSPackageListResponse getOMDSPackageList(OMDSPackageListRequest parameters);

    ArcImageResponse getArcImage(ArcImageRequest parameters);

    DeepLinkBusinessObjectResponse deepLinkClaim(DeepLinkClaimRequest parameters);

    OMDSPackageResponse getOMDSPackage(OMDSPackageRequest parameters);

    DeepLinkBusinessObjectResponse login(LoginRequestType parameters);

    DeepLinkBusinessObjectResponse deepLinkOffer(DeepLinkOfferRequest parameters);

    DeepLinkBusinessObjectResponse deepLinkPartner(DeepLinkPartnerRequest parameters);

    DeepLinkBusinessObjectResponse deepLinkPolicy(DeepLinkPolicyRequest parameters);

    ArcImageInfosResponse getArcImageInfos(ArcImageInfosRequest parameters);

    UserDataResponse getUserData(UserDataRequest parameters);

    GetNumberOfDocumentsResponseType getNumberOfDocuments(GetNumberOfDocumentsRequestType parameters);

    DeclareEndpointResponseType declareEndpoint(DeclareEndpointRequestType parameters);

    SearchClaimResponseType searchClaim(SearchClaimRequestType parameters);

    InitiateClaimResponseType initiateClaim(InitiateClaimRequestType parameters);

    CreateClaimResponseType createClaim(CreateClaimRequestType parameters);

    GetClaimResponseLightType getClaimLight(SpezifikationSchadenType parameters);

    ChangedClaimsListResponseType getChangedClaimsList(ChangedClaimsListRequestType parameters);

    GetDocumentInfosResponseType getDocumentInfos(GetDocumentInfosRequestType parameters);

    AddDocToClaimResponseType addDocToClaim(AddDocToClaimRequestType parameters);

    GetClaimResponseType getClaim(SpezifikationSchadenType parameters);

    LossEventListResponseType getLossEventList(LossEventListRequestType parameters);

}
