package at.vvo.omds.r1_3_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_3_0.on2antrag.kfz.*;

public interface KfzDataFetcher {

    CreateApplicationKfzResponseType createApplicationKfz(CreateApplicationKfzRequestType parameters);

    CreateOfferKfzResponseType createOfferKfz(CreateOfferKfzRequestType parameters);

    CalculateKfzResponseType calculateKfz(CalculateKfzRequestType parameters);

    SubmitApplicationKfzResponseType submitApplicationKfz(SubmitApplicationKfzRequestType parameters);

}
