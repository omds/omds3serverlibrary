package at.vvo.omds.r1_3_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_3_0.on1basis.*;

public interface BasisDataFetcher {

    GetStateChangesResponseType getStateChanges(GetStateChangesRequestType parameters);

}
