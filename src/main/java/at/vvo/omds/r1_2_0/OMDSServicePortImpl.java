package at.vvo.omds.r1_2_0;

import at.vvo.omds.r1_2_0.dataFetchers.ServiceTypesDataFetcher;
import at.vvo.omds.types.omds3Types.r1_2_0.service.OmdsServicePortType;
import at.vvo.omds.types.omds3Types.r1_2_0.service.ServiceFaultMsg;
import at.vvo.omds.types.omds3Types.r1_2_0.servicetypes.*;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;
import java.util.logging.Logger;

public class OMDSServicePortImpl implements OmdsServicePortType {

    @Resource
    protected WebServiceContext wsContext;

    private static final Logger LOGGER = Logger.getLogger(OMDSServicePortImpl.class.getName());

    private ServiceTypesDataFetcher serviceTypesDataFetcher;

    public OMDSServicePortImpl() {
    }

    protected void init(DataFetcherFactory dataFetcherFactory) {
        serviceTypesDataFetcher = dataFetcherFactory.generateServiceTypesDataFetcher();
    }

    /************************************************ SERVICE TYPES ************************************************/

    public OMDSPackageListResponse getOMDSPackageList(OMDSPackageListRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getOMDSPackageList");

        try {
            return serviceTypesDataFetcher.getOMDSPackageList(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public ArcImageResponse getArcImage(ArcImageRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getArcImage");

        try {
            return serviceTypesDataFetcher.getArcImage(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkClaim(DeepLinkClaimRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkClaim");

        try {
            return serviceTypesDataFetcher.deepLinkClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public OMDSPackageResponse getOMDSPackage(OMDSPackageRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getOMDSPackage");

        try {
            return serviceTypesDataFetcher.getOMDSPackage(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse login(LoginRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation login");

        try {
            return serviceTypesDataFetcher.login(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkOffer(DeepLinkOfferRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkOffer");

        try {
            return serviceTypesDataFetcher.deepLinkOffer(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkPartner(DeepLinkPartnerRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkPartner");

        try {
            return serviceTypesDataFetcher.deepLinkPartner(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkPolicy(DeepLinkPolicyRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkPolicy");

        try {
            return serviceTypesDataFetcher.deepLinkPolicy(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public ArcImageInfosResponse getArcImageInfos(ArcImageInfosRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getArcImageInfos");

        try {
            return serviceTypesDataFetcher.getArcImageInfos(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public UserDataResponse getUserData(UserDataRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getUserData");

        try {
            return serviceTypesDataFetcher.getUserData(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetNumberOfDocumentsResponseType getNumberOfDocuments(GetNumberOfDocumentsRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getNumberOfDocuments");

        try {
            return serviceTypesDataFetcher.getNumberOfDocuments(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeclareEndpointResponseType declareEndpoint(DeclareEndpointRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation declareEndpoint");

        try {
            return serviceTypesDataFetcher.declareEndpoint(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public SearchClaimResponseType searchClaim(SearchClaimRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation searchClaim");

        try {
            return serviceTypesDataFetcher.searchClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public InitiateClaimResponseType initiateClaim(InitiateClaimRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation initiateClaim");

        try {
            return serviceTypesDataFetcher.initiateClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public CreateClaimResponseType createClaim(CreateClaimRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createClaim");

        try {
            return serviceTypesDataFetcher.createClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetClaimResponseLightType getClaimLight(SpezifikationSchadenType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getClaimLight");

        try {
            return serviceTypesDataFetcher.getClaimLight(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public ChangedClaimsListResponseType getChangedClaimsList(ChangedClaimsListRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getChangedClaimsList");

        try {
            return serviceTypesDataFetcher.getChangedClaimsList(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetDocumentInfosResponseType getDocumentInfos(GetDocumentInfosRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getDocumentInfos");

        try {
            return serviceTypesDataFetcher.getDocumentInfos(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public AddDocToClaimResponseType addDocToClaim(AddDocToClaimRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation addDocToClaim");

        try {
            return serviceTypesDataFetcher.addDocToClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetClaimResponseType getClaim(SpezifikationSchadenType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getClaim");

        try {
            return serviceTypesDataFetcher.getClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public LossEventListResponseType getLossEventList(LossEventListRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getLossEventList");

        try {
            return serviceTypesDataFetcher.getLossEventList(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public abstract class DataFetcherFactory {

        public abstract ServiceTypesDataFetcher generateServiceTypesDataFetcher();

    }

}
