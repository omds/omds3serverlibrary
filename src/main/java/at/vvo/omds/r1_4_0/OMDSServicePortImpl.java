package at.vvo.omds.r1_4_0;

import at.vvo.omds.r1_4_0.dataFetchers.*;
import at.vvo.omds.types.omds3Types.r1_4_0.on1basis.*;
import at.vvo.omds.types.omds3Types.r1_4_0.on2antrag.kfz.*;
import at.vvo.omds.types.omds3Types.r1_4_0.service.OmdsServicePortType;
import at.vvo.omds.types.omds3Types.r1_4_0.service.ServiceFaultMsg;
import at.vvo.omds.types.omds3Types.r1_4_0.servicetypes.*;

import javax.annotation.Resource;
import javax.xml.ws.WebServiceContext;
import java.util.logging.Logger;

public class OMDSServicePortImpl implements OmdsServicePortType {

    @Resource
    protected WebServiceContext wsContext;

    private static final Logger LOGGER = Logger.getLogger(OMDSServicePortImpl.class.getName());

    private ServiceTypesDataFetcher serviceTypesDataFetcher;
    private BasisDataFetcher basisDataFetcher;
    private KfzDataFetcher kfzDataFetcher;

    public OMDSServicePortImpl() {
    }

    protected void init(DataFetcherFactory dataFetcherFactory) {
        serviceTypesDataFetcher = dataFetcherFactory.generateServiceTypesDataFetcher();
        basisDataFetcher = dataFetcherFactory.generateBasisDataFetcher();
        kfzDataFetcher = dataFetcherFactory.generateKfzDataFetcher();
    }

    /************************************************ SERVICE TYPES ************************************************/

    public OMDSPackageListResponse getOMDSPackageList(OMDSPackageListRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getOMDSPackageList");

        try {
            return serviceTypesDataFetcher.getOMDSPackageList(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public ArcImageResponse getArcImage(ArcImageRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getArcImage");

        try {
            return serviceTypesDataFetcher.getArcImage(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkClaim(DeepLinkClaimRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkClaim");

        try {
            return serviceTypesDataFetcher.deepLinkClaim(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public OMDSPackageResponse getOMDSPackage(OMDSPackageRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getOMDSPackage");

        try {
            return serviceTypesDataFetcher.getOMDSPackage(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse login(LoginRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation login");

        try {
            return serviceTypesDataFetcher.login(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkOffer(DeepLinkOfferRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkOffer");

        try {
            return serviceTypesDataFetcher.deepLinkOffer(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkPartner(DeepLinkPartnerRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkPartner");

        try {
            return serviceTypesDataFetcher.deepLinkPartner(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeepLinkBusinessObjectResponse deepLinkPolicy(DeepLinkPolicyRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation deepLinkPolicy");

        try {
            return serviceTypesDataFetcher.deepLinkPolicy(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public ArcImageInfosResponse getArcImageInfos(ArcImageInfosRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getArcImageInfos");

        try {
            return serviceTypesDataFetcher.getArcImageInfos(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public UserDataResponse getUserData(UserDataRequest parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getUserData");

        try {
            return serviceTypesDataFetcher.getUserData(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }


    /************************************************ BASIS ************************************************/

    public AddDocToBusinessCaseResponseType addDocToBusinessCase(AddDocToBusinessCaseRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation addDocToBusinessCase");

        try {
            return basisDataFetcher.addDocToBusinessCase(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetStateChangesResponseType getStateChanges(GetStateChangesRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getStateChanges");

        try {
            return basisDataFetcher.getStateChanges(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetNumberOfDocumentsResponseType getNumberOfDocuments(GetNumberOfDocumentsRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getNumberOfDocuments");

        try {
            return basisDataFetcher.getNumberOfDocuments(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetDocumentsOfPeriodResponseType getDocumentsOfPeriod(GetDocumentsOfPeriodRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getDocumentsOfPeriod");

        try {
            return basisDataFetcher.getDocumentsOfPeriod(parameters);
        } catch (Exception ex) {

            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public GetDocumentsOfObjectResponseType getDocumentsOfObject(GetDocumentsOfObjectRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation getDocumentsOfObject");

        try {
            return basisDataFetcher.getDocumentsOfObject(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public DeclareEndpointResponseType declareEndpoint(DeclareEndpointRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation declareEndpoint");

        try {
            return basisDataFetcher.declareEndpoint(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }


    /************************************************ KFZ ************************************************/

    public CreateApplicationKfzResponseType createApplicationKfz(CreateApplicationKfzRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createApplicationKfz");

        try {
            return kfzDataFetcher.createApplicationKfz(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public CreateOfferKfzResponseType createOfferKfz(CreateOfferKfzRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation createOfferKfz");

        try {
            return kfzDataFetcher.createOfferKfz(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public CalculateKfzResponseType calculateKfz(CalculateKfzRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation calculateKfz");

        try {
            return kfzDataFetcher.calculateKfz(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }

    public SubmitApplicationKfzResponseType submitApplicationKfz(SubmitApplicationKfzRequestType parameters) throws ServiceFaultMsg {
        LOGGER.info("Executing operation submitApplicationKfz");

        try {
            return kfzDataFetcher.submitApplicationKfz(parameters);
        } catch (Exception ex) {
            throw new ServiceFaultMsg(ex.getMessage(), ex);
        }
    }


    public abstract class DataFetcherFactory {

        public abstract ServiceTypesDataFetcher generateServiceTypesDataFetcher();

        public abstract BasisDataFetcher generateBasisDataFetcher();

        public abstract KfzDataFetcher generateKfzDataFetcher();

    }

}
