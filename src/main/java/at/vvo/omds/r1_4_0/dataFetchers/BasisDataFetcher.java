package at.vvo.omds.r1_4_0.dataFetchers;

import at.vvo.omds.types.omds3Types.r1_4_0.on1basis.*;

public interface BasisDataFetcher {

    AddDocToBusinessCaseResponseType addDocToBusinessCase(AddDocToBusinessCaseRequestType parameters);

    GetStateChangesResponseType getStateChanges(GetStateChangesRequestType parameters);

    GetNumberOfDocumentsResponseType getNumberOfDocuments(GetNumberOfDocumentsRequestType parameters);

    GetDocumentsOfPeriodResponseType getDocumentsOfPeriod(GetDocumentsOfPeriodRequestType parameters);

    GetDocumentsOfObjectResponseType getDocumentsOfObject(GetDocumentsOfObjectRequestType parameters);

    DeclareEndpointResponseType declareEndpoint(DeclareEndpointRequestType parameters);

}
