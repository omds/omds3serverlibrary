# OMDS3ServerLibrary

Enthält sog. DatenFetcher. Das sind Wrapper für die 
Serviceaufrufe ins (simulierte) Backend, um diese in verschiedenen
Versionen der WSDL nutzen zu könnnen.

Diese Wrapper werden im Branch für mehrere parallele WSDLs des Demo-Server verwendet, 
um auf die OMD3 Versionen anzubinden.

Dieses Projekt referenziert das OMDS 3 Objektmodell OMDSServiceDefinition.



